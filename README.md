# Tutorial: reusable-pipelines-lib


## Preparation

### 1. Install package from Git repo

```bash
export repo_token=<gitlab_repo_access_token>
export reusable_pipelines_lib_version=<version_of_repi=[tag|branch|commit_hash]>
pip install git+https://oauth2:${repo_token}gitlab.com:mnrozhkov/reusable-pipelines-lib.git@${reusable-pipelines-lib_version}
```

or install


```bash
pip install -v git+https://gitlab.com/mnrozhkov/reusable-pipelines-lib.git
```


```bash
pip install -e /Users/mnrozhkov/dev/mlrepa/reusable-pipelines/reusable-pipelines-lib  
```